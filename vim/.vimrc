"-------------------------------------------
" James Plante's Minimal Vimrc for getting
" stuff done.
" (jplante@wpi.edu)
"------------------------------------------

" Vundle
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins "call vundle#begin('~/some/path/here') " let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo

Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub

Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

Plugin 'scrooloose/nerdtree'
" NERDTREE Plugin for interactive file manager.

Plugin 'scrooloose/syntastic'
" Automatic syntax checking

Plugin 'ajmwagar/vim-deus'
" Deus Color Theme

Plugin 'lifepillar/vim-solarized8'
" Solarized Color Theme

Plugin 'bling/vim-airline'
" Airline Plugin

Plugin 'rip-rip/clang_complete'
" Clang C\C++ Autocomplete

Plugin 'zyedidia/vim-snake'
" Snake game

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

set exrc
set secure

" Basic formatting
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
set encoding=utf-8

" Set relative line numbers
set relativenumber

set listchars=tab:!-

" Automatic Parenthesis Completion
ino " ""<left>
ino ' ''<left>
ino ( ()<left>
ino [ []<left>
ino { {}<left>
ino {<CR> {<CR>}<ESC>O
ino {;<CR> {<CR>};<ESC>O

" Ctrl-d to switch to shell and back
noremap <C-d> :sh<cr>

" H - Tab left, L - tab right
noremap H gT
noremap L gt

" Color Scheme
colors deus

" Set the lines up to 80 characters.
set colorcolumn=80
highlight ColorColumn ctermbg=darkgrey

" Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Extended fonts
let g:airline_powerline_fonts = 1

" Disable preview window in clang_complete
set completeopt-=preview

" path to directory where library can be found
let g:clang_library_path='/usr/lib/llvm-8/lib/libclang.so.1'

" Enable automatic spellchecking
" set spell spelllang=en_us
